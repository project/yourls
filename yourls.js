(function ($) {
Drupal.verticalTabs = Drupal.verticalTabs || {};

Drupal.behaviors.yourlsFieldsetSummaries = {
  attach: function(context) {
    $('fieldset.yourls-settings-form', context).drupalSetSummary(function (context) {
      if($('#edit-yourls-existing-yourl').size()) {
        //set summary to existing short url
        return $('#edit-yourls-existing-yourl .yourls-short-url').html();
      }
      else if($('#edit-yourls-automatic').size())
      {
        return Drupal.t('No short URL');
      }
      else
      {
        return '';
      }
    });
  }
};

})(jQuery);
