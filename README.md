# Drupal Yourls Integration

Allows for the automatic creation of short urls using a Yourls installation. Simply select which content types should have short urls generated and they are generaeted on the next update or when a new node is created.

## Features

- Automatic short url retrieval / generation
- Stores urls in database to minimize calls to Yourls installation
- Ability to refresh the cache on a node or site level to ensure that short urls match the Yourls server

## Planned Features

- Allowing the ability to set a keyword for a short url when creating a node

##Note

Until this module is moved to Drupal.org, make sure that the module folder is "yourls" not "drupal-yourls".
